#include "rnastructure.h"
#include<string>
#include "Vector.h"
#include"Set.h"
#include <iostream.h>
#include <fstream>
#include <ctime>
#include <cmath>
#include <sstream>
#include "SimpleGraph.h"
#include <map.h>
#include <queue>


using namespace std;

class compareSet{
public:
    bool operator()(set <pair<int, int> >& LHS, set <pair <int, int> >& RHS){
        if (LHS.size()>=RHS.size()) return true;
        return false;
    }
};

RNAStructure::RNAStructure(ifstream& input){
    generate(input);
}



void RNAStructure:: generate(ifstream& input){
    vector<set<int> > intePositions;
    initiate(input,intePositions);
    m_interests=intePositions;
    set<int> available =m_available;
    set<pair<int, int> >  empty;
    int order=0;//
    priority_queue <  set<pair<int, int> >,set<pair<int, int> >, compareSet > structures;
    for (int m=0; m<2;m++){
    while (order!=m_interests.size()) {
        set<int> ::iterator it1=(m_interests[order]).begin();
        while (it1!=(m_interests[order]).end()) {

            int num=structures.size();
            int index=*it1;
            set<pair<int, int> > copyPair0;
            copyPair0=coinStructure(available, empty, index);
            structures.push(copyPair0);
            for (size_t i=0; i<num; i++ ){

                set<pair<int, int> >  basePairs;
                set<pair<int, int> > copyPair0;
                basePairs=structures.top();
                structures.pop();
                structures.emplace(basePairs);
                set<int> available =m_available;
                updateAvailable(basePairs,available);

                copyPair0=coinStructure(available, basePairs, index);
                if (copyPair0> basePairs)structures.emplace(copyPair0);

            }
            it1++;
        }
        order++;

    }
    order=0;
    m++;
    }

    int pairs=0;
    priority_queue <set<pair<int, int> >> structures1;
    do { empty=structures.top();

        structures1.push(empty);
        if(empty.size()>=pairs){
            pairs=empty.size();}
        structures.pop();
    }
    while (!structures.empty() );

    do { empty=structures1.top();\
        if(pairs-empty.size()<=2*MIN_PAIRS){
            cout << empty.size()<<endl;
            updateStruct(empty);}
        structures1.pop();
    }

    while (!structures1.empty());
    return;
}


void RNAStructure::initiate (ifstream& input, vector<set<int> >& intePositions){
    m_loopPair.push_back( "UA") ;
    m_loopPair.push_back( "TA") ;
    m_loopPair.push_back("AA");
    m_loopPair.push_back( "AG") ;
   // m_loopPair.push_back( "AU") ;
   // m_loopPair.push_back( "AT") ;
  //  m_loopPair.push_back( "GA");

    for (int i=0; i<m_loopPair.size(); i++){
        set<int> values;
        intePositions.push_back(values);
    }

    char A='A';
    char T='T';
    char U='U';
    char G='G';
    char C='C';
    vector<char> Acomplements;
    Acomplements.push_back(U);
    Acomplements.push_back(T);
    m_antisense[A]= Acomplements;

    vector<char> Gcomplements;
    Gcomplements.push_back(C);
    Gcomplements.push_back(U);
    Gcomplements.push_back(T);
    m_antisense[G]= Gcomplements;

    vector<char> Tcomplements;
    Tcomplements.push_back(A);
    Tcomplements.push_back(G);
    m_antisense[T]=Tcomplements;

    vector<char> Ucomplements;
    Ucomplements.push_back(A);
    Ucomplements.push_back(G);
    m_antisense[U]= Ucomplements;

    vector<char> Ccomplements;
    Ccomplements.push_back(G);
    m_antisense[C] =Ccomplements;
    Ccomplements.clear();
    Tcomplements.clear();
    Ucomplements.clear();
    Acomplements.clear();
    Gcomplements.clear();
    string line;
    string twos;
    pair<int,char> point;
    while (!input.eof()){
        getline(input, line);
        cout << line<< endl;
        int len=line.length();
        for (int i=0; i< len; i++){
            point.first=i;
            point.second=line[i];
            m_sequence.push_back(line[i]);
            int num=m_sequence.size();
            m_available.insert(num-1);
            twos=line.substr(i,2);
            for (int j=0; j<m_loopPair.size(); j++){
                if (twos==m_loopPair[j]){
                    (intePositions.at(j)).insert(num-1);// sequence size=index of value
                    m_numInterests++;
                }
            }
        }
    }
    m_length=m_sequence.size()-1;
    initiateGraph();
}


/**
 * @brief RNAStructure::initiateGraph
 *copies the m_sequence into a graph
 */
void RNAStructure::initiateGraph(){
    inCircle(m_length+3,graph);
}

//sets up the nodes vector in graph
void RNAStructure:: inCircle(const int num, SimpleGraph& graph){
    const double pi= 3.14159265358979323;
    int i=0;
    vector< char>::iterator it=m_sequence.begin();
    while (it!=m_sequence.end()){
        Node node;
        Edge edge;
        double xValue = (cos((2*pi*i)/num));
        double yValue = (sin((2*pi*i)/num));
        node.x=xValue;
        node.y=yValue;
        node.type=*it;
        graph.nodes.push_back(node);
        if (i==0 ){
            edge.start=i;}
        else {
            if (i==num){
                edge.end=edge.start;
                edge.start=0;
                graph.edges.push_back(edge);
            }else{
                edge.end=edge.start;
                edge.start=i;
                graph.edges.push_back(edge);
            }
        }
        it++;
        i++;
    }
    DrawGraph(graph);
}

set<pair<int, int> >  RNAStructure:: coinStructure( set<int> available,  set<pair<int, int> > basePairs ,  int index){
    symmetry(available, index, basePairs); //should return optimal solution
    return basePairs;
}

void RNAStructure:: updateAvailable(set<pair<int, int> >& basePairs,set<int>& available ){
    set <pair<int, int> >::iterator it=basePairs.begin();
    while (it!=basePairs.end()){
        pair <int, int> bond=*it;
        set<int>::iterator it2=available.find(min(bond.first, bond.second));
        if (it2!=available.end()){
            available.erase(it2);
            it2=available.find(max(bond.first, bond.second));
            if (it2!=available.end()){
                available.erase(it2);
            }
        }
        it++;
    }
}




void RNAStructure:: symmetry( set<int> available,int index,  set<pair<int, int> >& basePairs){//TO DO
   set <pair<int, int> > copyPairs=basePairs;//previous pairs
   set <pair<int, int> > prevPairs=basePairs;
   if (index==0||index==m_length) return;
   else{
       for(int ai=max(index-RANGE,0); ai<min(index+RANGE, m_length); ai++){
           set<int>::iterator it=available.find(ai);
           if(it!=available.end()){
               vector <char> complements=m_antisense.at(m_sequence[ai]);
               for (int bij= max(ai-RANGE,0); bij<min((ai+RANGE), m_length); bij++){
                   if ((bij>ai&&(bij-ai)>(index-ai) )||( bij<ai&& (bij-ai)<(index-ai))){
                       if (toCheck(bij, complements, available)){
                           copyPairs=countBase(min(ai,bij), max(ai, bij), basePairs, available);
                           if(copyPairs.size()>prevPairs.size())  {
                               prevPairs=copyPairs;
                           }
                       }
                   }
               }}
       }
       if(prevPairs.size()>basePairs.size())  {
           basePairs=prevPairs;
       }
   }
}


set<pair<int, int> >  RNAStructure::countBase(int start,int end,  set <pair <int, int> > basePairs, set<int>  available){
    pair<int, int> bond;
    bond.first=start;
    bond.second=end;
    basePairs.insert(bond);
    available.erase(available.find(start));
     available.erase(available.find(end));
    if (start-1>=0 && end+1<=m_length) {
        set<int>::iterator it=available.find(start-1);
        if (it!=available.end()){
            vector<char> complements=m_antisense[m_sequence[start-1]];
            if (toCheck (end+1, complements, available)) {
                basePairs=countBase(start-1, end+1, basePairs,available);
                return basePairs;
            }}
    }
    return basePairs;
}


/**
 * @brief RNAStructure::toCheck
 * @param index
 * @param complements
 * @return
 *checks if the index should be checked for potential pairing with another
 */
bool RNAStructure::toCheck(int index, vector <char>& complements,const set<int> & available){
    if (index<0|| index>m_length) return false;
    char ch=m_sequence[index];
    vector<char>::iterator it=complements.begin();
    if (it!=complements.end()&& *it==ch){
        set<int>::iterator it2=available.find(index);
        if (it2!=available.end()){
            return true;
        }
    }
    return false;
}



void RNAStructure:: updateStruct(const set <pair<int, int> >& basePairs){
    SimpleGraph graphCopy=graph;
    set <pair<int, int> >::iterator it=basePairs.begin();
    while (it!=basePairs.end()){
        graphCopy.nodes.at((*it).first).bond=(*it).second;
        graphCopy.nodes.at((*it).second).bond=(*it).first;
        Edge edge;
        edge.start=(*it).first;
        edge.end=(*it).second;
        graphCopy.edges.push_back(edge);\
        it++;
    }
    //optimize(graphCopy,basePairs);
    DrawGraph(graphCopy);
}


//return a vector for relative to node1
pair <double, double> RNAStructure:: getVector(Node node1, Node node2){
    pair<double, double> position;
    double x1=node1.x;
    double x2=node2.x;
    double y1=node1.y;
    double y2=node2.y;
    position.first= (pow((x1 -x2), 2.0) +pow(( y1 - y2), 2.0));
    position.second=atan2(y1-y2,x1-x2);
    return position;
}

// takes a pair of nodes and returns the force between them
pair<double, double>RNAStructure:: getForce(Node node1, Node node2,double KAttract){
    pair<double,double> force;
    pair<double,double> position=getVector(node1,node2);
    double forceTotal=-(kRepel/sqrt(position.first)) +(KAttract/pow(position.first,2.0));
    force.first =forceTotal*cos(position.second);
    force.second =forceTotal*sin(position.second);
    return force;
}

//returns total distance change after all the force
void RNAStructure:: totalForce(Node& node1, Node& node2, Node& node3, Node& node4){
    pair<double, double> totalForce;
    pair<double, double> totalForce1;
    pair<double, double> totalForce2;
    totalForce= getForce(node1, node2,1.0*kAttract);
    totalForce1=getForce(node1, node3,1.0*kAttract);
    totalForce2=getForce(node1, node4,2.75*kAttract);



    double delX=totalForce.first;
    double delY=totalForce.second;
    node2.x=node2.x-delX;
    node2.y=node2.y-delY;

    delX=totalForce1.first;
    delY=totalForce1.second;
    node3.x=node3.x-delX;
    node3.y=node3.y-delY;

    delX=totalForce2.first;
    delY=totalForce2.second;
    node4.x=node4.x-delX;
    node4.y=node4.y-delY;

//    double x =totalForce.first+totalForce1.first+totalForce2.first;
//    double y =totalForce.second+totalForce1.second+totalForce2.second;

    double x=totalForce2.first;
    double y=totalForce2.second;

    node1.x=node1.x+ x;
    node1.y=node1.y+ y;
}


void RNAStructure:: optimize(SimpleGraph& graphy, const set<pair<int, int> >& basePairs){
    set <pair<int, int> >::iterator it=basePairs.begin();
        while (it!=basePairs.end()){
            graphy.nodes.at((*it).first).bond=(*it).second;
            graphy.nodes.at((*it).second).bond=(*it).first;
            it++;
        }
        reposition(graphy);
}



void RNAStructure:: reposition(SimpleGraph& graphy){
    vector<Node>::iterator it=graphy.nodes.begin();
    time_t startTime=time(NULL);
    double elapsedTime=difftime(time(NULL), startTime);
    while (elapsedTime!=m_time){
        while (it!=graphy.nodes.end()){
            Node node1=(*it);
            Node node2;
            Node node3;
            Node node4;
            if(((*it).bond<=m_length) && ((*it).bond>=0)){node4 =graphy.nodes.at((*it).bond);
            if(it!=graphy.nodes.begin()) node2=*(it-1);
            if(it!=graphy.nodes.end()) node3=*(it+1);
            totalForce(node1, node2, node3, node4);
            *(it)=node1;
            if( ((*it).bond<=m_length) && ((*it).bond>=0)) graphy.nodes.at((*it).bond)=node4;
            *(it-1)=node2;
            *(it+1)=node3;}
            it++;
        }
        DrawGraph(graphy);
        it=graphy.nodes.begin();
        elapsedTime=difftime(time(NULL), startTime);
    }
}




/**
void RNAStructure:: optimize(SimpleGraph& graphy, const set<pair<int, int> >& basePairs){
    //pair<double, double> force;
    set <pair<int, int> >::iterator it=basePairs.begin();
    cout <<graphy.nodes.at((*it).first).x
        << " " << graphy.nodes.at((*it).first).y<< endl;
    time_t startTime=time(NULL);
    double elapsedTime=difftime(time(NULL), startTime);
    while (elapsedTime!=m_time){
        while (it!=basePairs.end()){
            Node node1=graphy.nodes.at((*it).first);
            Node node2;
            Node node3;
            Node node4=graphy.nodes.at((*it).second);
            if(!(*it).first==0){
                node2=graphy.nodes.at((*it).first-1);
                if((*it).first!=m_length){
                    node3=graphy.nodes.at((*it).first+1);
                    totalForce(node1, node2, node3, node4);
                    graphy.nodes.at((*it).first)=node1;
                    graphy.nodes.at((*it).second)=node4;
                    graphy.nodes.at((*it).first-1)=node2;
                    graphy.nodes.at((*it).first+1)=node3;
                }
            }
            it++;
        }
        DrawGraph(graphy);
        it=basePairs.begin();
        elapsedTime=difftime(time(NULL), startTime);
    }
}

 */
